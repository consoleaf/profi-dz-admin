import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import store from './store'
import cookies from "cookiesjs"
import FileUpload from 'v-file-upload'

Vue.config.productionTip = false;
Vue.use(FileUpload);
window.cookies = cookies;

new Vue({
    store,
    render: h => h(App)
}).$mount('#app');
