import Vue from 'vue'
import Vuex from 'vuex'
import cookies from "cookiesjs"

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        loaded: false,
        config: null,
        state: "unauthorized",
        api_origin: (window.location.hostname !== "newadmin.profi-dz.ru") ? "https://api.profi-dz.ru" : "https://newapi.profi-dz.ru",
        password: "",
        course: null,
        page: "check",
        dark_theme: cookies("dark_theme")
    },
    mutations: {
        INIT(state) {
            if (cookies("password")) {
                state.password = cookies("password");
                this.dispatch("loadConfig");
            }
        },
        LOGOUT(state) {
            cookies({password: null});
            cookies({course: null});
            state.password = null;
            state.course = null;
            state.state = "unauthorized";
        },
        WRITE_CONFIG(state, config) {
            state.config = config;
            state.loaded = true;
        },
        WRITE_PASSWORD(state, password) {
            state.password = password;
            cookies({password: password})
        },
        WRITE_STATE(state, newstate) {
            state.state = newstate;
        },
        WRITE_COURSE(state, course) {
            state.config.courses.forEach((v) => {
                if (v.origin === course)
                    state.course = v;
            });
            cookies({course: course});
        },
        SWITCH_PAGE(state, page) {
            state.page = page;
            cookies({page: page});
        },
        SWITCH_THEME(state) {
            let dark_theme = cookies("dark_theme");
            if (dark_theme == null)
                dark_theme = false;
            else
                dark_theme = !dark_theme;
            cookies({dark_theme: dark_theme});
            state.dark_theme = dark_theme;
        },
        WRITE_LOADED(state, loaded) {
            state.loaded = loaded;
        }
    },
    actions: {
        loadConfig() {
            fetch(this.state.api_origin + "/admin/get/config.php?password=" + this.state.password).then((response) => {
                return response.json();
            }).then((json) => {
                if (json.result === "unauthorized") {
                    alert("Неверный пароль!");
                    return json;
                }
                this.commit("WRITE_CONFIG", json);
                if (cookies("course")) {
                    this.commit("WRITE_COURSE", cookies("course"));
                    this.commit("WRITE_STATE", "main");

                    if (cookies("page"))
                        this.commit("SWITCH_PAGE", cookies("page"));
                } else
                    this.commit("WRITE_STATE", "choose");
                return json;
            });
        }
    }
})
